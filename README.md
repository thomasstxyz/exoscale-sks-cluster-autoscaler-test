https://community.exoscale.com/documentation/sks/autoscaling

## sks cluster

    exo compute sks create thomassttest3 --service-level pro --zone at-vie-1

## node pools

```
exo compute sks nodepool add thomassttest3 pool0 --instance-prefix pool0 --disk-size 20 --instance-type standard.small --zone at-vie-1 --size 1
exo compute sks nodepool add thomassttest3 pool1 --instance-prefix pool1 --disk-size 20 --instance-type standard.small --zone at-vie-1 --size 1 --label group=primary --taint group=primary:NoSchedule
exo compute sks nodepool add thomassttest3 pool2 --instance-prefix pool2 --disk-size 20 --instance-type standard.small --zone at-vie-1 --size 1 --label group=secondary --taint group=secondary:NoSchedule
exo compute sks nodepool add thomassttest3 pool3 --instance-prefix pool3 --disk-size 20 --instance-type standard.small --zone at-vie-1 --size 1 --label group=tertiary --taint group=tertiary:NoSchedule
```

## iam key for autoscaler

```
exo iam access-key create access_key_skscluster_thomassttest3 --operation "evict-sks-nodepool-members" --operation "get-instance" --operation "get-instance-pool" --operation "get-quota" --operation "list-sks-clusters" --operation "scale-sks-nodepool"
```

```
kubectl -n kube-system create secret generic ca-exoscale-api-credentials --from-literal=api-key="$EXOSCALE_API_KEY" --from-literal=api-secret="$EXOSCALE_API_SECRET" --from-literal=api-zone="$EXOSCALE_ZONE"
```
